<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('zipcode', 10)->nullable();
            $table->string('street_name',    60);
            $table->string('number',    10)->nullable();
            $table->string('complement',      60)->nullable();
            $table->string('neighborhood',     60)->nullable();
            $table->string('city', 60);
            $table->string('state', 60)->nullable();
            $table->integer('country')->nullable();
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
