/*global document, axios, FormData, CPF, Handlebars*/
(function(document, location, axios, FormData, CPF, Handlebars) {
  "use strict";
  var phoneLineTemplate = Handlebars.compile(
    document.getElementById("phone-line-template").innerText);

	function deleteLine(line) {
    var tbody = document.getElementById("phone_table").querySelector("tbody"),
      td = line.target.parentElement,
      root;
    td.parentElement.remove(td);
    if (tbody.querySelectorAll("tr").length === 0) {
      return addphoneLine();
    }
  }

	function addEventListener(class_name, method_id, event_id, func) {
    var i, elementList;
    elementList = document.getElementsByClassName(class_name);
    for (i=0; i<elementList.length; i+=1) {
      if (elementList.hasOwnProperty(i)) {
        elementList[i][method_id](event_id, func);
      }
    }
  }

	function removePhoneEventListenerToLine() {
    addEventListener("delete_phone_line", "removeEventListener", "click", deleteLine);
    addEventListener("phone", "removeEventListener", "input", formatPhone);
  }

  function addPhoneEventListenerToLine() {
    addEventListener("delete_phone_line", "addEventListener", "click", deleteLine);
    addEventListener("phone", "addEventListener", "input", formatPhone);
  }

	function addPhoneLine() {
    var tbody = document.getElementById("phone_table").querySelector("tbody"),
      i = tbody.querySelectorAll("tr").length,
      tr = tbody.insertRow(i);
    tr.innerHTML = phoneLineTemplate({"i": i});
    removePhoneEventListenerToLine();
    tbody.appendChild(tr);
    addPhoneEventListenerToLine();
  }

	function cleanUpElement(el) {
    var elParent = el.parentElement,
        childList = elParent.childNodes,
        i;
    elParent.className = "form-group";
    for (i in childList) {
      if (childList.hasOwnProperty(i) && childList[i].tagName == "SPAN") {
        elParent.removeChild(childList[i]);
        return cleanUpElement(el);
      }
    }
    return;
  }

  function addClassToElement(el, message, className) {
    var elParent = el.parentElement,
        span;

    cleanUpElement(el);
    elParent.className += " " + className;
    span = document.createElement("span");
    span.className = "help-block";
    span.innerHTML = message;
    elParent.appendChild(span);
    return;
  }

  function addErrorToElement(el, message) {
    return addClassToElement(el, message, "has-error");
  }

	function formatPhone(evt) {
    var el = evt.target, value;
    if (!el.value){
      return;
    }

    // Remove tudo o que não é dígito
    value = el.value.replace(/\D/g, "");
    // Coloca parênteses em volta dos dois primeiros dígitos
    value = value.replace(/^(\d{2})(\d)/g, "($1) $2");
    // Coloca hífen entre o quarto e o quinto dígitos
    value = value.replace(/(\d)(\d{4})$/, "$1-$2");
    el.value = value;
  }

  function formatCPF() {
    var cpfFormatted, el = document.getElementById("cpf"),
      value = el.value;
    if (value !== "") {
      if (CPF.validate(value) === false) {
        addErrorToElement(el, "CPF inv&#225;lido.");
      } else {
        cpfFormatted = CPF.format(value);
        if (value !== cpfFormatted) {
          el.value = cpfFormatted;
        }
        cleanUpElement(el);
      }
    }
  }

	function handleForm(evt) {
    var formData, phones = [];
		evt.preventDefault();
		document.querySelector("button[type='submit']").disabled = true;
	  formData = new FormData(evt.target);
	  ["cpf", "email"].forEach(function(key) {
	    cleanUpElement(document.getElementById(key));
	  });
    axios.post('/api/users', formData)
    .then(function (response) {
      location.href = response.headers.location;
    })
    .catch(function (error) {
      var errors = error.response.data.errors;
      Object.keys(errors).forEach(function(key){
        addErrorToElement(document.getElementById(key), errors[key][0]);
      });
    })
    .then(function () {
      document.querySelector("button[type='submit']").disabled = false;
    });  
	}

  document.addEventListener("DOMContentLoaded", function(){
    document.getElementById("cpf").addEventListener("input", formatCPF);
    document.querySelector("form").addEventListener("submit", handleForm);
    document.getElementById("add_line").addEventListener("click", addPhoneLine);
		return addPhoneEventListenerToLine();
  });
}(document, location, axios, FormData, CPF, Handlebars));
