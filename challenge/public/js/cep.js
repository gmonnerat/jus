/*global document, axios, Event*/
!(function (document, axios, Event) {
  "use strict";

  var cancelToken = axios.CancelToken,
    ufList = ["AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO",
              "MA", "MT", "MS", "MG", "PR", "PB", "PA", "PE", "PI",
              "RJ", "RN", "RS", "RO", "RR", "SC", "SE", "SP", "TO"],
    cancel;

  function addWarningToElement(el, message) {
    var zipcodeParent = el.parentElement,
        span;

    cleanUpElement(el);
    zipcodeParent.className += " has-error";
    span = document.createElement("span");
    span.className = "help-block";
    span.innerHTML = message;
    zipcodeParent.appendChild(span);
    return;
  }

  function getCEP() {
    var zipcode = document.getElementById("zipcode"),
        cep = zipcode.value.replace("-", ""),
        url = "https://viacep.com.br/ws/" + cep + "/json/",
        httpRequest;
    if (cancel) {
      cancel();
    }
    if (cep) {
      axios.get(url, {
        cancelToken: new cancelToken(function executor(c) {
          cancel = c;
        })
      })
      .then(populateAddress)
      .catch(function (error) {
        return addWarningToElement(zipcode, "CEP n&#227;o encontrado ou inv&#225;lido.");
      });
    }
  }

  function cleanUpElement(el) {
    var zipcodeParent = el.parentElement,
        childList = zipcodeParent.childNodes,
        i;
    zipcodeParent.className = "form-group";
    for (i in childList) {
      if (childList.hasOwnProperty(i) && childList[i].tagName == "SPAN") {
        zipcodeParent.removeChild(childList[i]);
        return cleanUpElement(el);
      }
    }
    return;
  }

  function addListener() {
    document.getElementById("zipcode").addEventListener("change", getCEP);
  }

  function populateAddress(response) {
    var el = document.getElementById("zipcode"),
        streetNameEl = document.getElementById("street_name"),
        evt = new Event('change'),
        data = response.data;
    cleanUpElement(el);
    if (data.erro) {
      return addWarningToElement(el, "CEP n&#227;o encontrado ou inv&#225;lido.");
    }
    document.getElementById("zipcode").value = data.cep;
    streetNameEl.value = data.logradouro;
    document.getElementById("city").value = data.localidade;
    document.getElementById("state").value = data.uf;
    document.getElementById("neighborhood").value = data.bairro;
    streetNameEl.dispatchEvent(evt);
    if (ufList.indexOf(data.uf) > -1) {
      document.getElementById("country").value = "Brasil";
    }
  }
  document.addEventListener("DOMContentLoaded", addListener);

}(document, axios, Event));
