<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'street',
        'street_extra',
        'city',
        'state',
        'post_code',
        'country',
    ];

		public function user() {
       return $this->belongsTo('App\User');
    }

    public function print()
    {
      return $this->street_name . ", " . $this->number . " - " . $this->complement . " CEP " . $this->zipcode . " " . $this->city;
    }

}
