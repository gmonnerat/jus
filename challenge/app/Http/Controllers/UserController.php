<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Phone;
use App\Address;


class UserController extends Controller
{
  // API
  public function index()
  {
    return User::all();
  }

  public function register(Request $request)
  {
		$this->validate($request, [
    	'email' => 'bail|email|required|unique:users',
    	'name' => 'required',
		  'cpf' => 'bail|required|unique:users',
    	'street_name' => 'required',
    	'city' => 'required',
      'phones' => 'required',
    ]);
    
    $user = new User;
		$user->name = $request->name;
 		$user->email = $request->email;
  	$user->cpf = $request->cpf;
    $user->password = Hash::make(str_random(16));
    $user->save();

    $address = new Address;
    $address->zipcode = $request->zipcode;
    $address->street_name = $request->street_name;
    $address->number = $request->number;
    $address->complement = $request->complement;
    $address->neighborhood = $request->neighborhood;
    $address->city = $request->city;
    $address->state = $request->state;
    $address->country = $request->country;
    $address->user_id = $user->id;
    $address->save();
    
    $phones = $request->phones;
    foreach ($phones as $number) {
      $phone = new Phone;
      $phone->phone = $number;
      $phone->user_id = $user->id;
      $phone->save(); 
    }
   
    return response()
      ->json($user, 201)
      ->header("Location", route('checkout', ['id' => $user->id]));
  }

  public function charge(Request $request)
  {
		$this->validate($request, [
    	'plan' => 'required',
    	'total_amount' => 'required',
		  'credit_card' => 'required',
      'user_id' => 'required'
    ]);
 
		$user = User::findOrFail($request->user_id);
    if ($user->paid) {
      return response()
        ->json(["errors" => ["credit_card" => ["Pagamento já foi realizado"]]], 422);
    };

    $credit_card = $request->credit_card;
    if ($credit_card == "1234") {
      $paid = 1;
    } elseif ($credit_card == "1111") {
      $paid = 0;
    } else {
      $paid = array_rand([1, 0]);
    }

    if ($paid) {
      $user->paid = 1;
      $user->save();
      $payment_status = "success";
      $http_response = 200;
    } else {
      $payment_status = "failed";
      $http_response = 422;
    }

    return response()
      ->json(["payment_status" => $payment_status], $http_response);
  }

  // Backend
  public function checkout($id)
  {
    $user = User::findOrFail($id);
    return view('user.checkout', ['user' => $user,
      'address' => $user->address,
      'phones' => $user->phones,
    ]);
  }

  public function status($id)
  {
    $user = User::findOrFail($id);
    return view('user.status', [
      'status' => ($user->paid ? 'Pago' : 'Pagamento em aberto'),
    ]);
  } 
}
