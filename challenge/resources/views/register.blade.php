<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cadastro</title>
				<script id="phone-line-template" type="text/x-handlebars-template">
					<tr>
						<td>
							<input name='phones[]' type='text' class='form-control phone' required>
						</td>
						<td><i class="fas fa-trash-alt delete_phone_line" aria-hidden="true"></i></td>
					</tr>
				</script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">

				<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
				<!-- jQuery library -->
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

				<!-- Popper JS -->
				<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>

				<!-- Latest compiled JavaScript -->
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.1.0/handlebars.min.js"></script>
        <script src="js/cep.js"></script>
        <script src="js/CPF.min.js"></script>
        <script src="js/register.js"></script>
        <link rel="stylesheet" href="/css/all.css">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="top-right links">
              <a href="/">Back to Home</a>
            </div>
            <div class="content">
              <form method="POST">
                {{ csrf_field() }}
								<div class="row">
									<div class="col-lg-6">
										<h3>Dados Pessoais</h3>
										<div class="form-group">
											<input placeholder="Nome" type="text" class="form-control" id="name" name="name" required>
										</div>
										<div class="form-group">
											<input placeholder="Email" type="email" class="form-control" id="email" name="email" required>
										</div>
										<div class="form-group">
											<input placeholder="CPF" type="text" class="form-control" id="cpf" name="cpf" required>
                    </div>
										<div class="form-group">
											<label class="control-label">Telefones</label>
											<table id="phone_table">
												<tbody>
													<tr>
														<td>
															<input name="phones[]" type="text" class="form-control phone" required="">
														</td>
														<td><i class="fas fa-trash-alt delete_phone_line" aria-hidden="true"></i></td>
													</tr>
												</tbody>
										 	</table>
										 	<div class="row">
											 	<div class="col-md-4">
												 	<a id="add_line"><i class="fas fa-plus"></i></a>
											 	</div>
										 	</div>
										</div>
									</div>
									<div class="col-lg-6">
										<h3>Endereço</h3>
										<div class="form-group">
											<input type="text" name="zipcode" autocomplete="off" class="form-control" placeholder="CEP" id="zipcode">
										</div>
										<div class="form-group">
											<input type="text" name="street_name" autocomplete="off" class="form-control" placeholder="Endereço" id="street_name">
										</div>
										<div class="form-group">
											<input type="text" name="number" autocomplete="off" class="form-control" placeholder="Número" id="number">
										</div>
										<div class="form-group">
											<input type="text" name="complement" autocomplete="off" class="form-control" placeholder="Complemento" id="complement">
										</div>
										<div class="form-group">
											<input type="text" name="neighborhood" autocomplete="off" class="form-control" placeholder="Bairro" id="neighborhood">
										</div>
										<div class="form-group">
											<input type="text" name="city" autocomplete="off" class="form-control" placeholder="Cidade" id="city">
										</div>
										<div class="form-group">
											<input type="text" name="state" autocomplete="off" class="form-control" placeholder="Estado" id="state">
										</div>
										<div class="form-group">
											<input type="text" name="country" autocomplete="off" class="form-control" placeholder="País" id="country">
										</div>    
									</div>
									<div class="form-group">
										<button style="cursor:pointer" type="submit" class="btn btn-primary">Cadastrar</button>
									</div>
								</div>
              </form>
            </div>
        </div>
    </body>
</html>
