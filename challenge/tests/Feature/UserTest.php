<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Address;
use App\Phone;


class UserTest extends TestCase
{
  use RefreshDatabase;
  public function testHome()
  {
    $this->get("/")
      ->assertStatus(200)
      ->assertSeeText("PHP Desafio")
      ->assertSeeText("Cadastrar");
  }

  public function testRegister()
  {
    $this->get("/register")
      ->assertStatus(200)
      ->assertSeeText("Dados Pessoais");

    $response = $this->json('POST', '/api/users',
      ['name' => 'Sally', "email" => "gabriel@example.com"]);
    $response
      ->assertStatus(422)
      ->assertJson([
        "message" => "The given data was invalid.",
        "errors" => [
          "cpf" => [
            "The cpf field is required."
          ]
        ]
      ]);

    $response = $this->json('POST', '/api/users',
      ['name' => 'Sally', "cpf" => "123"]);
    $response
      ->assertStatus(422)
      ->assertJson([
        "message" => "The given data was invalid.",
        "errors" => [
          "email" => [
            "The email field is required."
          ]
        ]
      ]);

    $response = $this->json('POST', '/api/users',
      ['name' => 'Sally', "cpf" => "123", "email" => "gabriel@example.com"]);
    $response
      ->assertStatus(422)
      ->assertJson([
        "message" => "The given data was invalid.",
        "errors" => [
          "city" => [
            "The city field is required."
          ]
        ]
      ]);

    $response = $this->json('POST', '/api/users',
      [
        'name' => 'Gabriel',
        "cpf" => "123",
        "email" => "gabriel@example.com",
        "city" => "Rio de Janeiro"
      ]);
    $response
      ->assertStatus(422)
      ->assertJson([
        "message" => "The given data was invalid.",
        "errors" => [
          "street_name" => [
            "The street name field is required."
          ]
        ]
      ]);

    $response = $this->json('POST', '/api/users',
      [
        'name' => 'Gabriel',
        "cpf" => "123",
        "email" => "gabriel@example.com",
        "city" => "Rio de Janeiro",
        "street_name" => "Avenida"
      ]);
    $response
      ->assertStatus(422)
      ->assertJson([
        "message" => "The given data was invalid.",
        "errors" => [
          "phones" => [
            "The phones field is required."
          ]
        ]
      ]);
    $this->assertEquals(0, User::all()->count());

    $response = $this->json('POST', '/api/users',
      [
        'name' => 'Gabriel',
        "cpf" => "123",
        "email" => "gabriel@example.com",
        "city" => "Rio de Janeiro",
        "street_name" => "Avenida",
        "phones" => array("21 999095870", "21 999095871"),
      ]);

    $response
      ->assertStatus(201)
      ->assertJson([
        "id" => 1,
        "name" => "Gabriel"
      ]);
    $response
      ->assertHeader("Location", "http://localhost/user/1/checkout");

    $this->get("/user/1/checkout")
      ->assertStatus(200)
      ->assertSeeText("Nome: Gabriel")
      ->assertSeeText("CPF: 123")
      ->assertSeeText("21 999095870");
    $this->assertEquals(1, User::all()->count());
    $user = User::findOrFail(1);
    $this->assertEquals(0, $user->paid);
    $this->assertEquals("gabriel@example.com", $user->email);
    $this->assertEquals(1, Address::all()->count());
    $this->assertEquals(2, $user->phones->count());

    $response = $this->json('POST', '/api/charge',
      [
        'plan' => 'advanced',
        "total_amount" => "20",
        "credit_card" => "1234",
        "user_id" => "1",
      ]);

    $response
      ->assertStatus(200);
    $user = User::findOrFail(1);
    $this->assertEquals(1, $user->paid);
 
  }

  public function testPaymentFail()
  {
    $response = $this->json('POST', '/api/users',
      [
        'name' => 'Gabriel',
        "cpf" => "123",
        "email" => "gabriel@example.com",
        "city" => "Rio de Janeiro",
        "street_name" => "Avenida",
        "phones" => array("21 999095870", "21 999095871"),
      ]);

    $response
      ->assertStatus(201)
      ->assertJson([
        "id" => 1,
        "name" => "Gabriel"
      ]);
    $response
      ->assertHeader("Location", "http://localhost/user/1/checkout");

    $user = User::findOrFail(1);
    $this->assertEquals(0, $user->paid);
    $response = $this->json('POST', '/api/charge',
      [
        'plan' => 'advanced',
        "total_amount" => "20",
        "credit_card" => "1111",
        "user_id" => "1",
      ]);

    $response
      ->assertStatus(422);
    $user = User::findOrFail(1);
    $this->assertEquals(0, $user->paid);
    $response = $this->json('POST', '/api/charge',
      [
        'plan' => 'advanced',
        "total_amount" => "20",
        "credit_card" => "1234",
        "user_id" => "1",
      ]);
    $user = User::findOrFail(1);
    $this->assertEquals(1, $user->paid);
  }
  public function testPaymentStatus()
  {
    $response = $this->json('POST', '/api/users',
      [
        'name' => 'Gabriel',
        "cpf" => "123",
        "email" => "gabriel@example.com",
        "city" => "Rio de Janeiro",
        "street_name" => "Avenida",
        "phones" => array("21 999095870", "21 999095871"),
      ]);

    $response
      ->assertStatus(201)
      ->assertJson([
        "id" => 1,
        "name" => "Gabriel"
      ]);
    $response
      ->assertHeader("Location", "http://localhost/user/1/checkout");
    $this->get("/user/1/status")
      ->assertStatus(200)
      ->assertSeeText("Pagamento em aberto");

    $user = User::findOrFail(1);
    $this->assertEquals(0, $user->paid);
    $response = $this->json('POST', '/api/charge',
      [
        'plan' => 'advanced',
        "total_amount" => "20",
        "credit_card" => "1111",
        "user_id" => "1",
      ]);

    $response
      ->assertStatus(422);
    $user = User::findOrFail(1);
    $this->assertEquals(0, $user->paid);
    $response = $this->json('POST', '/api/charge',
      [
        'plan' => 'advanced',
        "total_amount" => "20",
        "credit_card" => "1234",
        "user_id" => "1",
      ]);
    $user = User::findOrFail(1);
    $this->assertEquals(1, $user->paid);
    $this->get("/user/1/status")
      ->assertStatus(200)
      ->assertSeeText("Pago");
  }
}
