FROM php:7.3.2

COPY challenge /challenge

WORKDIR /challenge
RUN apt-get update \
  && apt-get install --no-install-recommends -y unzip sqlite3 \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* \
	&& sqlite3 /challenge/database.sqlite
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && php composer-setup.php && php -r "unlink('composer-setup.php');" && docker-php-ext-install pdo mbstring

RUN php composer.phar install \
  && php ./artisan migrate:refresh \
  && php artisan key:generate \
  && php artisan config:cache
CMD php ./artisan serve --host=0.0.0.0 --port=8000
EXPOSE 8000
HEALTHCHECK --interval=1m CMD curl -f http://localhost:8000/ || exit 1
